#include <iostream>
#include "pessoa.cpp"
#include "contato.cpp"
#include "amigo.cpp"

using namespace std;


int main () {
	
	Pessoa n;
	Contato x;
	amigo y;

	n.setNome("Paulo");
	n.setIdade("32");
	n.setTelefone("555-5555");
	x.setTrabalho("STJ");
	x.setFaculdade("UnB");
	y.setEndereco("Rua 12");
	y.setAniversario("13/10");
		
	cout << "Nome: " << n.getNome() << endl;
	cout << "Idade: " << n.getIdade() << endl;
	cout << "Telefone: " << n.getTelefone() << endl;
	cout << "Trabalho: " << x.getTrabalho() << endl;
    cout << "Faculdade: " << x.getFaculdade() << endl;
	cout << "Endereco: " << y.getEndereco() << endl;
    cout << "Aniversario: " << y.getAniversario() << endl;

	
	
	return 0;
}