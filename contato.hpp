#ifndef CONTATO_H
#define CONTATO_H

#include <iostream>
#include <string>
#include "pessoa.hpp" 

class Contato : public Pessoa {

private :
	string trabalho;
	string faculdade;

public :
	Contato ();
	Contato ( string trabalho, string faculdade );
	string getTrabalho ();
	void setTrabalho ( string trabalho );
	string getFaculdade();
	void setFaculdade( string faculdade );
};

#endif