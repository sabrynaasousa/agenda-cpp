#ifndef AMIGO_H
#define AMIGO_H

#include <iostream>
#include <string>
#include "pessoa.hpp" 

class amigo : public Pessoa {

private :
	string endereco;
	string aniversario;

public :
	amigo ();
	amigo ( string endereco, string aniversario );
	string getEndereco ();
	void setEndereco ( string endereco );
	string getAniversario();
	void setAniversario( string aniversario );
};

#endif
		